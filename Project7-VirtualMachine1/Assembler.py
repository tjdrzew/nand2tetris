# hack assembler

from sys import argv

symbolTable={
    'SP'   : '0',
    'LCL'  : '1',
    'ARG'  : '2',
    'THIS' : '3',
    'THAT' : '4',
     'R0':'0',   'R1':'1',   'R2':'2',   'R3':'3',
     'R4':'4',   'R5':'5',   'R6':'6',   'R7':'7',
     'R8':'8',   'R9':'9',  'R10':'10', 'R11':'11', 
    'R12':'12', 'R13':'13', 'R14':'14', 'R15':'15',
    'SCREEN' : '16384',
    'KBD' : '24576'
}

def clearWhiteSpace(line):
    a=line.split('//')     # split up any comments
    b=a[0].strip()         # eliminate any white space
    return b

def isACmd(cmd):
    if cmd[0] == '@':
        return True
    else:
        return False

def isLabel(cmd):
    if cmd[0] == '(':
        return True
    else:
        return False

def writeAcmd(cmd):
    tmp=cmd.split('@')
    loc=tmp[1]
    key = loc in symbolTable
    if key:
        res = int(symbolTable[loc])
    else:
        res = int(loc)
    binary = ''
    for i in reversed(range(16)):
        digit = 2**i
        if res < digit:
            binary = binary + '0'
        else:
            binary = binary + '1'
            res = res - digit
    return(binary)

def writeCcmd(cmd):
    acomp2bin = {
        '0'  : '0101010',
        '1'  : '0111111',
        '-1' : '0111010',
        'D'  : '0001100',
        'A'  : '0110000', 'M' : '1110000',
        '!D' : '0001101',
        '!A' : '0110001', '!M' : '1110001',
        '-D' : '0001111',
        '-A' : '0110011', '-M' : '1110011',
        'D+1': '0011111',
        'A+1': '0110111', 'M+1': '1110111',
        'D-1': '0001110',
        'A-1': '0110010', 'M-1': '1110010',
        'D+A': '0000010', 'D+M': '1000010',
        'D-A': '0010011', 'D-M': '1010011',
        'A-D': '0000111', 'M-D': '1000111',
        'D&A': '0000000', 'D&M': '1000000',
        'D|A': '0010101', 'D|M': '1010101'
    }
    dest2bin = {
        'null':'000',
         'M'  :'001',
          'D' :'010',
         'MD' :'011',
        'A'   :'100',
        'AM'  :'101',
        'AD'  :'110',
        'AMD' :'111'
    }

    jump2bin = {
        'null': '000',
        'JGT' : '001',
        'JEQ' : '010',
        'JGE' : '011',
        'JLT' : '100',
        'JNE' : '101',
        'JLE' : '110',
        'JMP' : '111'
    }

    # get acomp, and dest
    tmp=cmd.split('=')
    if (len(tmp) == 1):
        dest = 'null'
        rhs = tmp[0]
    else:
        dest = tmp[0]
        rhs  = tmp[1]  
    tmp=rhs.split(';')
    acomp=tmp[0]
 
    # get jump
    tmp=cmd.split(';')
    if (len(tmp)==1):
        jump='null'
    else:
        jump=tmp[1]
    
    #write the binary code for C-instruction
    binary = '111' + acomp2bin[acomp] + dest2bin[dest] + jump2bin[jump]
    return(binary)

inFile = argv[1]
AsmFile = open(inFile)

# scan for labels
lineno = 0
for line in AsmFile:
    cmd=clearWhiteSpace(line)
    if cmd!='':
        if cmd[0]=='(':
            label=cmd[1:(len(cmd)-1)]
            symbolTable[label]=str(lineno)
        else:
            lineno=lineno+1

# scan for variables
AsmFile.seek(0)
varAdd = 16
for line in AsmFile:
    cmd=clearWhiteSpace(line)
    if cmd!='':
        Acmd = isACmd(cmd)
        if (Acmd):
            if cmd[1].isalpha():
                var = cmd[1:len(cmd)]
                key = var in symbolTable
                if not(key):
                    symbolTable[var] = str(varAdd)
                    varAdd = varAdd + 1

# convert assembly to binary
AsmFile.seek(0)
tmp = inFile.split('.')
outFile = tmp[0] + '.hack'
HackFile = open(outFile,'w')
for line in AsmFile:
    cmd=clearWhiteSpace(line)
    if cmd!='':
        Acmd = isACmd(cmd)
        LBL  = isLabel(cmd)
        if (Acmd):
            Ains = writeAcmd(cmd)
            HackFile.write(Ains+'\n')
        elif not LBL:
            Cins = writeCcmd(cmd)
            HackFile.write(Cins+'\n')

AsmFile.close()   
HackFile.close()     

