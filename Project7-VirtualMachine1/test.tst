load test.asm,
output-file test.out,
compare-to test.cmp,
output-list RAM[256]%D1.6.1 RAM[300]%D1.6.1 RAM[401]%D1.6.1 
            RAM[402]%D1.6.1 RAM[3006]%D1.6.1 RAM[3012]%D1.6.1
            RAM[3015]%D1.6.1 RAM[11]%D1.6.1;

set RAM[0] 256,  // initializes the stack pointer
set RAM[1] 300,  // local
set RAM[2] 400,  // argument
set RAM[3] 3000, // this
set RAM[4] 3010, // that

repeat 300 {    // enough cycles to complete the execution
  ticktock;
}

output;
