# Virtual Machine Translator

from sys import argv
import os

def clearWhiteSpace(line):
    tmp=line.split('//')     # split up any comments
    vmCode=tmp[0].strip()         # eliminate any white space
    if vmCode=='':
        isCode = False
    else:
        isCode = True
    return vmCode, isCode

def incrementSP():
    asmFile.write('@SP\n')
    asmFile.write('M=M+1\n')

def regD2Stack(): 
    # place Rediger D into stack
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')
    asmFile.write('M=D\n') 

def writePush(segment,loc):
    asmFile.write('// push ' + segment + ' ' + loc + '\n') 
    if segment == 'constant':
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        regD2Stack()
        incrementSP()
    elif segment == 'local':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # select register at (LCL + loc) and place value into D
        asmFile.write('@LCL\n')
        asmFile.write('A=D+M\n')
        asmFile.write('D=M\n')
        regD2Stack()
        incrementSP()
    elif segment == 'argument':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # select register at (ARG + loc) and place value into D
        asmFile.write('@ARG\n')
        asmFile.write('A=D+M\n')
        asmFile.write('D=M\n')
        regD2Stack()
        incrementSP()
    elif segment == 'this':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # select register at (THIS + loc) and place value into D
        asmFile.write('@THIS\n')
        asmFile.write('A=D+M\n')
        asmFile.write('D=M\n')
        regD2Stack()
        incrementSP()
    elif segment == 'that':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # select register at (THAT + loc) and place value into D
        asmFile.write('@THAT\n')
        asmFile.write('A=D+M\n')
        asmFile.write('D=M\n')
        regD2Stack()
        incrementSP()
    elif segment == 'pointer':
        # place value from pointer location into D register
        reg = 3 + int(loc)
        acmd = '@' + str(reg)
        asmFile.write(acmd + '\n')
        asmFile.write('D=M\n')
        # place value in D register into the stack
        regD2Stack()
        incrementSP()   
    elif segment == 'temp':
        # place value from temp location into D register
        reg = 5 + int(loc)
        acmd = '@' + str(reg)
        asmFile.write(acmd + '\n')
        asmFile.write('D=M\n')
        # place value in D register into the stack
        regD2Stack()
        incrementSP()
    elif segment == 'static':
        # place value from static location into D register
        acmd = '@' + root + '.' + loc
        asmFile.write(acmd + '\n')
        asmFile.write('D=M\n')
        # place value in D register into the stack
        regD2Stack()
        incrementSP()

def writePop(segment,loc):
    asmFile.write('// pop ' + segment + ' ' + loc + '\n')
    if segment == 'local':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # icrease LCL by loc
        asmFile.write('@LCL\n')
        asmFile.write('M=D+M\n')
        # place stack into D register
        stack2RegD()
        # place D register into register pointed to by LCL
        asmFile.write('@LCL\n')
        asmFile.write('A=M\n')
        asmFile.write('M=D\n')
        # return value of LCL by decrasing by loc
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        asmFile.write('@LCL\n')
        asmFile.write('M=M-D\n')                       
    elif segment == 'argument':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # icrease ARG by loc
        asmFile.write('@ARG\n')
        asmFile.write('M=D+M\n')
        # place stacck into D register
        stack2RegD()
        # place D register into register pointed to by ARG
        asmFile.write('@ARG\n')
        asmFile.write('A=M\n')
        asmFile.write('M=D\n')
        # return value of ARG by decreasing by loc
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        asmFile.write('@ARG\n')
        asmFile.write('M=M-D\n')   
    elif segment == 'this':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # icrease THIS by loc
        asmFile.write('@THIS\n')
        asmFile.write('M=D+M\n')
        # place stacck into D register
        stack2RegD()
        # place D register into register pointed to by THIS
        asmFile.write('@THIS\n')
        asmFile.write('A=M\n')
        asmFile.write('M=D\n')
        # return value of THIS by decreasing by loc
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        asmFile.write('@THIS\n')
        asmFile.write('M=M-D\n')   
    elif segment == 'that':
        # push loc into register D
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        # icrease THAT by loc
        asmFile.write('@THAT\n')
        asmFile.write('M=D+M\n')
        # place stacck value into D register
        stack2RegD()
        # place D register into register pointed to by THAT
        asmFile.write('@THAT\n')
        asmFile.write('A=M\n')
        asmFile.write('M=D\n')
        # return value of THIS by decreasing by loc
        asmFile.write('@'+loc+'\n')
        asmFile.write('D=A\n')
        asmFile.write('@THAT\n')
        asmFile.write('M=M-D\n') 
    elif segment == 'pointer':
         # place stack value into D register
        stack2RegD()
        # place D register into pointer location       
        reg = 3 + int(loc)
        acmd = '@' + str(reg)
        asmFile.write(acmd + '\n')
        asmFile.write('M=D\n')
    elif segment == 'temp':
         # place stack value into D register
        stack2RegD()
        # place D register into pointer location       
        reg = 5 + int(loc)
        acmd = '@' + str(reg)
        asmFile.write(acmd + '\n')
        asmFile.write('M=D\n')
    elif segment == 'static':
         # place stack value into D register
        stack2RegD()
        # place D register into pointer location       
        acmd = '@' + root + '.' + loc
        asmFile.write(acmd + '\n')
        asmFile.write('M=D\n')

# Stack arithmetic functions

def setupStackArith2():
    # place top of stack into D register
    asmFile.write('@SP\n')
    asmFile.write('M=M-1\n')
    asmFile.write('A=M\n')
    asmFile.write('D=M\n')
    # select the register pointed to 
    #   by the top of the stack
    asmFile.write('@SP\n')
    asmFile.write('M=M-1\n')
    asmFile.write('A=M\n')

def setupStackArith1():
    # place top of stack into D register
    asmFile.write('@SP\n')
    asmFile.write('M=M-1\n')
    asmFile.write('A=M\n')
    asmFile.write('D=M\n')

def stack2RegD():
    # place the top of the stack into Register D
    asmFile.write('@SP\n')
    asmFile.write('M=M-1\n')
    asmFile.write('A=M\n')
    asmFile.write('D=M\n') 

def writeAdd():
    asmFile.write('// add\n')
    setupStackArith2()
    asmFile.write('M=D+M\n')
    incrementSP()

def writeSub():
    asmFile.write('// sub\n')
    setupStackArith2()
    asmFile.write('M=M-D\n')
    incrementSP()

def writeNeg():
    asmFile.write('// neg')
    setupStackArith1()
    asmFile.write('M=-D\n')
    incrementSP()

def writeEq(eqID):
    asmFile.write('// eq\n')
    EQtag = 'EQ'+str(eqID)
    ENDtag = 'END_EQ' + str(eqID)
    eqID = eqID + 1
    setupStackArith2()
    asmFile.write('D=M-D\n')
    asmFile.write('@'+EQtag+'\n')
    asmFile.write('D;JEQ\n')   # jump to EQtag if D == 0
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')
    asmFile.write('M=0\n')    # set location to false then jump to end
    asmFile.write('@'+ENDtag+'\n')
    asmFile.write('0;JMP\n')
    asmFile.write('('+EQtag+')\n')
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')    
    asmFile.write('M=-1\n')
    asmFile.write('('+ENDtag+')\n')
    incrementSP()
    return eqID

def writeGt(gtID):
    asmFile.write('// gt\n')
    GTtag = 'GT'+str(gtID)
    ENDtag = 'END_GT' + str(gtID)
    gtID = gtID + 1
    setupStackArith2()
    asmFile.write('D=M-D\n')
    asmFile.write('@'+GTtag+'\n')
    asmFile.write('D;JGT\n')   # jump to tag if D > 0
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')
    asmFile.write('M=0\n')    # set location to false then jump to end
    asmFile.write('@'+ENDtag+'\n')
    asmFile.write('0;JMP\n')
    asmFile.write('('+GTtag+')\n')
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')    
    asmFile.write('M=-1\n')
    asmFile.write('('+ENDtag+')\n')
    incrementSP()
    return gtID    

def writeLt(ltID):
    asmFile.write('// lt\n')
    LTtag = 'LT'+str(ltID)
    ENDtag = 'END_LT' + str(ltID)
    ltID = ltID + 1
    setupStackArith2()
    asmFile.write('D=M-D\n')
    asmFile.write('@'+LTtag+'\n')
    asmFile.write('D;JLT\n')   # jump to tag if D < 0
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')
    asmFile.write('M=0\n')    # set location to false then jump to end
    asmFile.write('@'+ENDtag+'\n')
    asmFile.write('0;JMP\n')
    asmFile.write('('+LTtag+')\n')
    asmFile.write('@SP\n')
    asmFile.write('A=M\n')    
    asmFile.write('M=-1\n')
    asmFile.write('('+ENDtag+')\n')
    incrementSP()
    return ltID    

def writeAnd():
    asmFile.write('// and\n')
    setupStackArith2()
    asmFile.write('M=D&M\n')
    incrementSP()

def writeOr():
    asmFile.write('// or\n')
    setupStackArith2()
    asmFile.write('M=D|M\n')
    incrementSP()

def writeNot():
    asmFile.write('// not\n')
    setupStackArith1()
    asmFile.write('M=!D\n')
    incrementSP()

# Program Flow Functions
def writeIfGoto(LABEL):
    asmFile.write('// if-goto ' + LABEL + '\n')
    setupStackArith1()
    asmFile.write('@'+LABEL+'\n')
    asmFile.write('D;JNE\n')

def writeGoto(LABEL):
    asmFile.write('// goto ' + LABEL + '\n')
    asmFile.write('@'+LABEL+'\n')
    asmFile.write('0;JMP\n')

def writeLabel(LABEL):
     asmFile.write('// label ' + LABEL +'\n')
     asmFile.write('(' + LABEL + ')\n')
     
# Main Program
inFile = os.path.basename(argv[1])
dir    = os.path.dirname(argv[1])
[root,ext] = inFile.split('.')
inFile = os.path.join(dir,inFile)
outFile = root + '.asm'
outFile = os.path.join(dir,outFile)
vmFile = open(inFile)
asmFile = open(outFile,'w') 

eqID = 0
gtID = 0
ltID = 0
for line in vmFile:
    [vmCode, isCode] = clearWhiteSpace(line)
    if isCode:
        cmd = vmCode.split(' ')
        if cmd[0] == 'push':
            writePush(cmd[1],cmd[2])
        elif cmd[0] == 'pop':
            writePop(cmd[1],cmd[2])
        elif cmd[0] ==  'add':
            writeAdd()
        elif cmd[0] ==  'sub':
            writeSub()
        elif cmd[0] ==  'neg':
            writeNeg() 
        elif cmd[0] ==  'eq':
            eqID = writeEq(eqID)
        elif cmd[0] ==  'gt':
            gtID=writeGt(gtID)
        elif cmd[0] ==  'lt':
            ltID = writeLt(ltID)
        elif cmd[0] == 'and':
            writeAnd()
        elif cmd[0] ==  'or':
            writeOr()
        elif cmd[0] ==  'not':
            writeNot()    
        elif cmd[0] == 'label':
            writeLabel(cmd[1])  
        elif cmd[0] == 'goto':
            writeGoto(cmd[1])                                                                                        
        elif cmd[0] == 'if-goto':
            writeIfGoto(cmd[1])           

vmFile.close()
asmFile.close()