// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
//
// This program only needs to handle arguments that satisfy
// R0 >= 0, R1 >= 0, and R0*R1 < 32768.

    @sum  // set sum equal to zero
    M=0
    @i    // set i equal to zero
    M=0  
    @R2   // set R2 equal to zero
    M=0
(LOOP)
// Check to see if at the end of the loop
    @i
    D=M
    @R1
    D=D-M
    @END
    D;JEQ

// Add R0 to sum
    @R0
    D=M
    @sum
    M=D+M
// increment i and return to top of loop
    @i
    M=M+1
    @LOOP
    0;JMP

(END)
    //Place sum into R2 and end program
    @sum
    D=M
    @R2
    M=D
    @END
    0;JMP