// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

(LOOP)
    @KBD
    D=M
    @BLACK
    D;JNE  
    @WHITE0
    0;JMP

(BLACK)
// set index to screen
    @SCREEN
    D=A
    @i
    M=D
(BLKLOOP)
// check if i is equal to KBD
    @KBD
    D=A
    @i
    D=D-M
    @LOOP
    D;JEQ
// if i < KBD, increase i and set the next address to black
    @i
    A=M
    M=-1
    @i
    M=M+1
    @BLKLOOP
    0;JMP


(WHITE0)
// set index to screen
    @SCREEN
    D=A
    @i
    M=D
(WHTLOOP)
// check if i is equal to KBD
    @KBD
    D=A
    @i
    D=D-M
    @LOOP
    D;JEQ
// if i < KBD, increase i and set the next address to white
    @i
    A=M
    M=0
    @i
    M=M+1
    @WHTLOOP
    0;JMP